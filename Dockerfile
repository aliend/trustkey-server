FROM ubuntu:16.04

RUN apt-get update && \
    apt-get -y install nginx build-essential cmake nano libboost-all-dev \
    libconfig++8-dev libssl-dev && \
    mkdir /app

WORKDIR "/app"

COPY src /app/src

RUN cd src && cmake . && make -j1 && cp TrustKeyApiServer .. && mv schemas .. \
    && cd .. && rm -rf src

COPY entrypoint.sh /app/entrypoint.sh
#COPY nginx.conf /etc/nginx/nginx.conf

RUN chmod +x entrypoint.sh

EXPOSE 18770
EXPOSE 18771

CMD ["/app/entrypoint.sh"]
