#ifndef API_APISERVER_HPP
#define API_APISERVER_HPP

//Todo: PIMPl

#include <restbed>
#include "TrustKeyService.hpp"
#include "JsonRequestBodyValidator.hpp"
#include "JsonGenerator.hpp"

class HttpApiServer {
    TrustKeyService* trustKeyService;

    JsonGenerator jsonGenerator;
    JsonRequestBodyValidator& validator;
    const TrustKeyConfiguration& configuration;
    restbed::Service service;
    shared_ptr<asio::io_service> restbedService;

    void getInfoHandler(shared_ptr<restbed::Session> session);
    void postInputHash(shared_ptr<restbed::Session> session);
    void postInput(shared_ptr<restbed::Session> session);
public:
    void setTrustKeyService(TrustKeyService *trustKeyService);

    HttpApiServer(JsonRequestBodyValidator &validator,
                  const TrustKeyConfiguration &configuration,
                  JsonGenerator& jsonGenerator);

    void start(bool detached = false);
};

#endif //API_APISERVER_HPP