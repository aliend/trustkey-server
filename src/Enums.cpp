#include "Enums.hpp"

std::string errors[] = {
    "",                              //0
    "No content ",                   //1
    "No Content-Length header",      //2
    "Wrong length of input",         //3
    "Message body validation error", //4
    "Hash not found",                //5
};