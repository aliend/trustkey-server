#ifndef TRUSTKEYAPISERVER_TRUSTKEYSERVERFACADE_H
#define TRUSTKEYAPISERVER_TRUSTKEYSERVERFACADE_H


#include "TrustKeyConfiguration.hpp"
#include "JsonGenerator.hpp"
#include "TrustKeyService.hpp"
#include "JsonRequestBodyValidator.hpp"
#include "HttpApiServer.hpp"
#include <string>

class TrustKeyServerFacade {
    TrustKeyConfiguration configuration;
    JsonGenerator jsonGenerator;
    JsonRequestBodyValidator jsonValidator;
    TrustKeyDb trustKeyDb;
    ~TrustKeyServerFacade() = default; //Services stop methods are unimplemented
public:
    TrustKeyServerFacade(): trustKeyDb(configuration) {}
    void run(const std::string &configFilePath) {
        try {
            configuration.loadFromFile(configFilePath);
            trustKeyDb.loadTkDb();

            HttpApiServer httpApiServer(jsonValidator, configuration, jsonGenerator);
            WebsocketServer websocketServer(jsonGenerator, configuration, jsonValidator);

            TrustKeyService trustKeyService(
                    configuration,
                    jsonGenerator,
                    websocketServer,
                    trustKeyDb,
                    httpApiServer
            );

            websocketServer.start(true);
            httpApiServer.start(true);

            trustKeyService.start(false);
        }
        catch (string s) {
            cerr << "Fatal exception:" << endl
                 << s << endl;
        }
    }
};


#endif //TRUSTKEYAPISERVER_TRUSTKEYSERVERFACADE_H
