#include <iostream>

#include "gtest/gtest.h"
#include "../MultiIndexDigestSet.hpp"

class TrustKeyTest : public ::testing::Test {
protected:
    TrustKeyTest() {    }
    virtual ~TrustKeyTest() {    }
    virtual void SetUp() {     }
    virtual void TearDown() {    }
};

TEST_F(TrustKeyTest, MultiIndexDigestSet)
{
    MultiIndexDigestSet multiIndexDigestSet;
    auto& indexByHash = multiIndexDigestSet.container.get<IndexByHash>();
    auto& indexById = multiIndexDigestSet.container.get<IndexByDigestId>();

    char input[] = "test";
    char input2[] = "test2";
    char input3[] = "test3";

    TrustKeyInput trustKeyInput(reinterpret_cast<unsigned char *>(input), sizeof(input));
    auto digestPtr = trustKeyInput.calculate();

    TrustKeyInput trustKeyInput2(reinterpret_cast<unsigned char *>(input2), sizeof(input2));
    auto digest2Ptr = trustKeyInput2.calculate();

    TrustKeyInput trustKeyInput3(reinterpret_cast<unsigned char *>(input3), sizeof(input3));
    auto digest3Ptr = trustKeyInput3.calculate();


    ASSERT_EQ(true, multiIndexDigestSet.tryInsertDigest(*digestPtr));
    ASSERT_EQ(true, multiIndexDigestSet.tryInsertDigest(*digest2Ptr));
    ASSERT_EQ(false, multiIndexDigestSet.tryInsertDigest(*digest2Ptr));

    ASSERT_EQ(true, multiIndexDigestSet.tryInsertInput(trustKeyInput2));
    ASSERT_EQ(true, multiIndexDigestSet.tryInsertInput(trustKeyInput));
    ASSERT_EQ(false, multiIndexDigestSet.tryInsertInput(trustKeyInput2));
    ASSERT_EQ(false, multiIndexDigestSet.tryInsertInput(trustKeyInput3));

    size_t iteration = 0;

    multiIndexDigestSet.iterateByDigestIdIndex([&](const DigestInputRecord& record) {
        switch (iteration) {
            case 0:
                ASSERT_EQ(0, record.digestId);
                ASSERT_EQ(record.digest, *digestPtr);
                ASSERT_EQ(record.input, trustKeyInput);
                break;
            case 1:
                ASSERT_EQ(1, record.digestId);
                ASSERT_EQ(record.digest, *digest2Ptr);
                ASSERT_EQ(record.input, trustKeyInput2);
                break;
            default:
                ASSERT_TRUE(false);
        }
        ++iteration;
    });

    //Todo: similar digest case
}