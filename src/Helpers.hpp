#ifndef API_HELPERS_HPP
#define API_HELPERS_HPP

#include <sys/time.h>>
#include <string>

namespace h {
    inline double currentTimeMs() {
        struct timeval tp;
        gettimeofday(&tp, NULL);
        double s = tp.tv_sec;
        double us = tp.tv_usec;
        us /= 1000000.f;
        s += us;
        return s;
    }

    inline std::string sCurrentTimeMs(double t = currentTimeMs()) {
        std::string s(16, '0');
        int written = std::snprintf(&s[0], s.size(), "%.2f", t);
        s.resize(written);
        return s;
    }
}

#endif //API_HELPERS_HPP
