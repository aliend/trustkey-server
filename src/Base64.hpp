#ifndef _BASE64_H_
#define _BASE64_H_
#include <string>

namespace Base64 {
    std::string encode(unsigned char const* , unsigned int len);
    std::string decode(const char* encoded_string, size_t in_len);
}

#endif