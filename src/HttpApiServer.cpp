#include <restbed>
#include <iostream>
#include <memory>
#include <rapidjson/rapidjson.h>
#include <rapidjson/writer.h>
#include <rapidjson/document.h>
#include <boost/lexical_cast.hpp>

#include "HttpApiServer.hpp"
#include "Helpers.hpp"
#include "JsonBodyParser.hpp"
#include "Enums.hpp"

using namespace restbed;
using namespace std;
using namespace rapidjson;

using boost::lexical_cast;

void HttpApiServer::start(bool detached) {
    assert(trustKeyService);

    validator.addScheme("post_hash", "post_hash.json");

    service.add_rule(make_shared<JsonBodyParser>());

    //GET /server_info
    {
        auto resource = make_shared< Resource >( );
        resource->set_path( "/server_info" );
        resource->set_method_handler( "GET", bind(&HttpApiServer::getInfoHandler, this, placeholders::_1));
        service.publish( resource );
    }

    //POST /current/hashes
    {
        auto resource = make_shared< Resource >( );
        resource->set_path( "/current/hashes" );
        resource->set_method_handler( "POST", bind(&HttpApiServer::postInputHash, this, placeholders::_1));
        service.publish( resource );
    }

    //POST /current/inputs
    {
        auto resource = make_shared< Resource >( );
        resource->set_path( "/current/inputs" );
        resource->set_method_handler( "POST", bind(&HttpApiServer::postInput, this, placeholders::_1));
        service.publish( resource );
    }


    restbedService = service.get_io();

    auto workerFoo = [this]() {
        //Todo: stop implementation
        auto settings = make_shared< Settings >( );
        settings->set_port(configuration.httpServerPort);
        settings->set_default_header( "Connection", "close" );

        service.start( settings );
    };

    if(detached)
        thread(workerFoo).detach();
    else
        workerFoo();
}

void HttpApiServer::getInfoHandler(const shared_ptr<restbed::Session> session) {
    const auto request = session->get_request( );

    session->close(200, jsonGenerator.standardResponse(
            true, bind(&JsonGenerator::writeServerInfo, jsonGenerator, placeholders::_1, configuration)));
}

void HttpApiServer::postInputHash(const shared_ptr<restbed::Session> session) {
    shared_ptr<Document> jsonBodyPtr = session->get("json_body", nullptr);
    bool validationRes = validator.validateDocument("post_hash", *jsonBodyPtr);

    if(!jsonBodyPtr || !validationRes) {
        session->close(400);
        return;
    }

    Document& jsonBody = *jsonBodyPtr;
    string digestString = jsonBody["sha512_hash"].GetString();
    boost::optional<Sha512Digest> digest = Sha512Digest::fromHexString(digestString);

    if(!digest) {
        session->close(400); //Todo: bad body
        return;
    }

    static const multimap<string, string> headers {
            { "Content-Type", "application/json" }
    };

    trustKeyService->pushHash(*digest, [this, session](size_t nextTrustKeyTs) {
            restbedService->post([this, nextTrustKeyTs, session]() {
                session->close(200, jsonGenerator.postHashResponse(nextTrustKeyTs), headers);
            });
        }, false);
}

void HttpApiServer::postInput(const shared_ptr<restbed::Session> session) {
    size_t content_length;
    auto request = session->get_request();
    content_length = lexical_cast<size_t>(request->get_header( "Content-Length",  "0"));

    if(content_length == 0) {
        session->yield(400, jsonGenerator.errorResponse(ErrorCode::CONTENT_LENGHT_ERROR));
        return;
    }

    session->fetch( content_length,
    [this]( const shared_ptr< Session > session,
                           const Bytes & body)
    {
        if(body.size() != configuration.inputLength) {
            session->yield(400, jsonGenerator.errorResponse(ErrorCode::WRONG_INPUT_LENGTH));
            return;
        }

        unsigned char* data = (unsigned char *) malloc(body.size());
        memcpy(data, &body[0], body.size());

        trustKeyService->pushInput(TrustKeyInput(data, body.size()),
           [this, session(move(session))](PushInputError error, size_t currTrustKeyId) {
               restbedService->post([this, error, currTrustKeyId, session(move(session))]() {
                   session->close(200, jsonGenerator.postInputResponse(error, currTrustKeyId));
               });
           }, false);
    });
}

HttpApiServer::HttpApiServer(JsonRequestBodyValidator &validator, const TrustKeyConfiguration &configuration,
                             JsonGenerator& jsonGenerator)
        : validator(validator), configuration(configuration), jsonGenerator(jsonGenerator) {}

void HttpApiServer::setTrustKeyService(TrustKeyService *trustKeyService) {
    HttpApiServer::trustKeyService = trustKeyService;
}