#include "DeferredWriter.hpp"

void DeferredWriter::worker() {
    shared_ptr<DeferredWriter> protector = shared_from_this();
    boost::filesystem::create_directories(boost::filesystem::path(filePath).parent_path().string().c_str());
    fdout = open(filePath.c_str(), O_CREAT | O_WRONLY, fileMode);

    while(true) {
        m.lock();
        if(tasks.size() > 0) {
            list<WriteTask> copy(move(tasks));
            m.unlock();
            for(auto it = copy.rbegin(); it != copy.rend(); ++it) {
                WriteTask& t = *it;
                write(fdout, t.ptr, t.len);
                delete t.ptr;
            }
        }
        else {
            m.unlock();

            if(protector.use_count() == 1) {
                protector = nullptr;
                break;
            }
        }

        //Todo: conditional variables use
        usleep(500 * 1000);
    }
}

shared_ptr<DeferredWriter> DeferredWriter::create(std::string filePath, mode_t fileMode) {
    return shared_ptr<DeferredWriter>(new DeferredWriter(USE_CREATE_METHOD{}, filePath, fileMode),
                                      [](DeferredWriter* self)
                                      {
                                          ::close(self->fdout);
                                      });
}

void DeferredWriter::startWorker() {
    thread(&DeferredWriter::worker, this)
            .detach();
}
