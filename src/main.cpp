#include "TrustKeyServerFacade.hpp"
#include "MultiIndexDigestSet.hpp"
#include <args.hxx>


int main(const int argc, const char** argv) {
    args::ArgumentParser parser("TrustKey API Server");
    args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    args::CompletionFlag completion(parser, {"complete"});
    args::ValueFlag<string> configPath(parser, "config", "Config file path", {'c', "config"}, "tkey.conf");

    try
    {
        parser.ParseCLI(argc, argv);
    }
    catch (args::Completion e)
    {
        std::cout << e.what();
        return 0;
    }
    catch (args::Help)
    {
        std::cout << parser;
        return 0;
    }
    catch (args::ParseError e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    (new TrustKeyServerFacade())->run(configPath.Get());

    return EXIT_SUCCESS;
}