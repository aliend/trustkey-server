#ifndef TRUSTKEYAPISERVER_RESPONSEGENERATOR_HPP
#define TRUSTKEYAPISERVER_RESPONSEGENERATOR_HPP

#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <functional>
#include <string>

#include "Enums.hpp"
#include "JsonGenerator.hpp"
#include "TrustKeyConfiguration.hpp"

class JsonGenerator {
public:
    using SbWriter = rapidjson::Writer<rapidjson::StringBuffer>;
    using ObjectWriterCallback = std::function<void(SbWriter& w)>;

    JsonGenerator() = default;
    //Writers
    void writeServerInfo(SbWriter& w, const TrustKeyConfiguration& configuration) const;
    void writeMessageHeader(SbWriter &w,
                            std::string type,
                            size_t msgId,
                            ObjectWriterCallback c = [](SbWriter &w) {}) const;

    //Http responses
    std::string standardResponse(bool success, ObjectWriterCallback c = [](SbWriter& w){}) const;
    std::string postHashResponse(size_t nextTrustKeyTs, ObjectWriterCallback c = [](SbWriter& w){}) const;
    std::string postInputResponse(PushInputError error,
                                  size_t currTrustKeyTs,
                                  JsonGenerator::ObjectWriterCallback c = [](SbWriter& w){}) const;

    std::string errorResponse(int errCode, std::string error, ObjectWriterCallback c  = [](SbWriter& w){}) const;
    std::string errorResponse(ErrorCode errCode, ObjectWriterCallback c  = [](SbWriter& w){}) const;

    //Websocket messages
    std::string wsStandardMessage(std::string type, ObjectWriterCallback c) const;

    std::string roundSync(size_t currTrustKeyId, size_t nextTrustKeyId, double t) const;

    std::string wsHash(std::string hashHex, double t) const;
    std::string wsInput(std::string inputBase64, double t) const;
    std::string wsTrustKey(size_t tkId, std::string trustKeyHex, double t) const;
};

#endif //TRUSTKEYAPISERVER_RESPONSEGENERATOR_HPP