#include "ServerInputGenerator.hpp"

ServerInputGenerator::ServerInputGenerator(size_t inputLength) : inputLength(inputLength) {}

TrustKeyInput ServerInputGenerator::generate() {
    auto* bytes = static_cast<uint8_t*>(operator new(inputLength));
    std::generate(bytes, bytes + inputLength, std::ref(rbe));
    return TrustKeyInput(bytes, inputLength);
}
