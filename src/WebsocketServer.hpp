#ifndef TRUSTKEYAPISERVER_WEBSOCKETSERVER_HPP
#define TRUSTKEYAPISERVER_WEBSOCKETSERVER_HPP

#include <memory>
#include <set>
#include <array>

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include "Enums.hpp"
#include "JsonGenerator.hpp"
#include "JsonRequestBodyValidator.hpp"

using websocketpp::connection_hdl;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;

class TrustKeyService;

class WebsocketConnection {
public:
    size_t msgId;
    std::array<bool, WebsocketQueryArrayLength> query;
    WebsocketConnection(): query() {
        msgId = 0;
        query[WebsocketMessageType::TRUST_KEY] = true;
    }
};

struct custom_config : public websocketpp::config::asio {
    // pull default settings from our core config
    typedef websocketpp::config::asio core;

    typedef core::concurrency_type concurrency_type;
    typedef core::request_type request_type;
    typedef core::response_type response_type;
    typedef core::message_type message_type;
    typedef core::con_msg_manager_type con_msg_manager_type;
    typedef core::endpoint_msg_manager_type endpoint_msg_manager_type;
    typedef core::alog_type alog_type;
    typedef core::elog_type elog_type;
    typedef core::rng_type rng_type;
    typedef core::transport_type transport_type;
    typedef core::endpoint_base endpoint_base;

    // Set a custom connection_base class
    typedef WebsocketConnection connection_base;
};

typedef websocketpp::server<custom_config> WsServer;

class WebsocketServer {
    std::shared_ptr<websocketpp::lib::asio::io_service> service;
    WsServer server;

    typedef websocketpp::message_buffer::message<websocketpp::message_buffer::alloc::con_msg_manager>
            message_type;
    typedef websocketpp::message_buffer::alloc::con_msg_manager<message_type>
            con_msg_man_type;

    typedef std::set<connection_hdl,std::owner_less<connection_hdl>> con_list;
    con_list m_connections;

    TrustKeyService* trustKeyService;
    con_msg_man_type::ptr messageManager;
    const JsonGenerator& jsonGenerator;
    JsonRequestBodyValidator& jsonValidator;
    const TrustKeyConfiguration& configuration;
public:
    WebsocketServer(const JsonGenerator& jsonGenerator, const TrustKeyConfiguration& configuration,
                    JsonRequestBodyValidator& jsonValidator)
            : jsonGenerator(jsonGenerator), configuration(configuration), jsonValidator(jsonValidator) {
        service = std::make_shared<websocketpp::lib::asio::io_service>(1);
        messageManager = websocketpp::lib::make_shared<con_msg_man_type>();
    }

    void setTrustKeyService(TrustKeyService *trustKeyService);
    void on_open(connection_hdl hdl);
    void on_close(connection_hdl hdl);
    void on_message(connection_hdl hdl, WsServer::message_ptr msg);
    void broadcast(WebsocketMessageType t, std::string message);
    void start(bool detached = false);
};


#endif //TRUSTKEYAPISERVER_WEBSOCKETSERVER_HPP
