#ifndef CONVSERV_JBP_HPP
#define CONVSERV_JBP_HPP

#include <restbed>

class JsonBodyParser : public restbed::Rule
{
public:
    JsonBodyParser( void ) : Rule( )
    { }

    virtual ~JsonBodyParser( void )
    { }

    bool condition( const std::shared_ptr< restbed::Session > session) final override
    {
        std::string contentType = session->get_request()->get_header("Content-Type", "");
        size_t index = contentType.find("application/json");
        return index != std::string::npos;
    }

    void action( const std::shared_ptr< restbed::Session > session,
                 const std::function< void ( const std::shared_ptr< restbed::Session > ) >& callback ) final override;
};


#endif