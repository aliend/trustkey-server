#include "JsonBodyParser.hpp"
#include "boost/lexical_cast.hpp"
#include <rapidjson/document.h>
#include <iostream>

using boost::lexical_cast;

using namespace restbed;
using namespace std;
using namespace rapidjson;

void
JsonBodyParser::action(const shared_ptr<Session> session, const function<void(const shared_ptr<Session>)> &callback) {
    size_t content_length;
    auto request = session->get_request();
    content_length = lexical_cast<size_t>(request->get_header( "Content-Length",  "0"));

    session->fetch( content_length,
                    [ request, callback ]( const shared_ptr< Session > session,
                                           const Bytes & body )
    {
        string bodyStr(body.begin(), body.end());
        cout << bodyStr << endl;
        auto document_ptr = make_shared<Document>();
        if (document_ptr->Parse(bodyStr.c_str()).HasParseError()) {
            session->close(400, "", { { "Content-Length", "0" }} );
            return;
        }
        session.get()->set("json_body", move(document_ptr));
        callback(move(session));
    });
}