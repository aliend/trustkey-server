//
// Created by origin on 04.02.17.
//

#include <iostream>
#include "JsonRequestBodyValidator.hpp"
using namespace std;

void JsonRequestBodyValidator::addScheme(string schemeKey, string schemeFileName) {
    std::ifstream t(string("schemas/") + schemeFileName);
    std::string schemaString((std::istreambuf_iterator<char>(t)),
                             std::istreambuf_iterator<char>());
    Document sd;
    if (sd.Parse(schemaString.c_str()).HasParseError()) {
        cerr << "Wrong schema" << endl;
        throw WrongSchemeException();
    }
    schemes.insert(make_pair(schemeKey, SchemaDocument(sd)));
}

bool JsonRequestBodyValidator::validateDocument(string schemeKey, const Document &document) {
    auto res = schemes.find(schemeKey);
    if(res == schemes.end())
        return false;
    SchemaValidator validator(res->second);
    bool vRes = document.Accept(validator);

    /*
    if (!vRes) {
        // Input JSON is invalid according to the schema
        // Output diagnostic information
        StringBuffer sb;
        validator.GetInvalidSchemaPointer().StringifyUriFragment(sb);
        printf("Invalid schema: %s\n", sb.GetString());
        printf("Invalid keyword: %s\n", validator.GetInvalidSchemaKeyword());
        sb.Clear();
        validator.GetInvalidDocumentPointer().StringifyUriFragment(sb);
        printf("Invalid document: %s\n", sb.GetString());
    }
    */

    return vRes;
}
