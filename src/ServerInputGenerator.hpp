#ifndef TRUSTKEYAPISERVER_SERVERINPUTGENERATOR_HPP
#define TRUSTKEYAPISERVER_SERVERINPUTGENERATOR_HPP

#include "Sha512.hpp"
#include <random>

class ServerInputGenerator {
    using random_bytes_engine = std::independent_bits_engine<
            std::default_random_engine, 8, uint32_t>;

    size_t inputLength;
    random_bytes_engine rbe;
public:
    explicit ServerInputGenerator(size_t inputLength);
    TrustKeyInput generate();
};

#endif //TRUSTKEYAPISERVER_SERVERINPUTGENERATOR_HPP