#include "TrustKeyConfiguration.hpp"

#include <iostream>
#include <boost/lexical_cast.hpp>
#include <libconfig.h++>

using namespace libconfig;

using namespace std;

template <class T>
bool tryLoad(Config& cfg, T& target, string name) {
    try
    {
        cfg.lookupValue(name, target);
    }
    catch(const SettingNotFoundException &nfex)
    {
        throw(string("No '") +  name + "' setting in configuration file.");
    }
    return true;
}

template <>
bool tryLoad<string>(Config& cfg, string& target, string name) {
    try
    {
        target =  cfg.lookup(name).c_str();
    }
    catch(const SettingNotFoundException &nfex)
    {
        throw(string("No '") +  name + "' setting in configuration file.");
    }
    return true;
}

void TrustKeyConfiguration::loadFromFile(const std::string& filePath) {
    Config cfg;

    try
    {
        cfg.readFile(filePath.c_str());
    }
    catch(const FileIOException &fioex)
    {
        throw(string("I/O error while reading configuration file('") + filePath + "')");
    }
    catch(const ParseException &pex)
    {
        throw(string("Parse error at ") + pex.getFile() + ":" + to_string(pex.getLine())
                                       + " - " + pex.getError());
    }

    //Todo: check if config value is positive
    tryLoad(cfg, (string&)this->trustKeyDbFileName, "trust_key_db_file_name");

    int iFileMode;
    tryLoad(cfg, iFileMode, "file_mode");
    this->fileMode = 0662; //static_cast<mode_t>(iFileMode); //Todo: use file node var

    tryLoad(cfg, this->serverId, "server_id");
    tryLoad(cfg, this->httpServerPort, "http_server_port");
    tryLoad(cfg, this->websocketServerPort, "websocket_server_port");
    tryLoad(cfg, this->roundTime, "round_time");
    tryLoad(cfg, this->inputLength, "input_length");
    tryLoad(cfg, this->serverInputLength, "server_input_length");
    tryLoad(cfg, this->storageDirPath, "storage_dir_path");
    tryLoad(cfg, this->hashesLogFileName, "hashes_log_file_name");
    tryLoad(cfg, this->inputsLogFileName, "inputs_log_file_name");
    tryLoad(cfg, this->inputsFileName, "inputs_file_name");
    tryLoad(cfg, this->sha512TrustKeyFileName, "sha512_trust_key_file_name");
}