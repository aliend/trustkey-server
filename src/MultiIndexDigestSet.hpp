#ifndef TRUSTKEYAPISERVER_DIGESTINPUTMULTIMAP_HPP
#define TRUSTKEYAPISERVER_DIGESTINPUTMULTIMAP_HPP

#include <iostream>
#include "DigestInputRecord.hpp"
#include "boost/unordered_map.hpp"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <gtest/gtest_prod.h>

struct IndexByHash {};
struct IndexByDigestId;

class MultiIndexDigestSet {
    FRIEND_TEST(TrustKeyTest, MultiIndexDigestSet);
    using Container = boost::multi_index_container<
            DigestInputRecord,
            boost::multi_index::indexed_by<
                boost::multi_index::ordered_unique<
                        boost::multi_index::tag<IndexByDigestId>,
                        BOOST_MULTI_INDEX_MEMBER(DigestInputRecord, size_t, digestId),
                        std::less<size_t>
                >,
                boost::multi_index::hashed_non_unique<
                        boost::multi_index::tag<IndexByHash>,
                        boost::multi_index::const_mem_fun<DigestInputRecord, size_t, &DigestInputRecord::digestUMapHash>
                >
            >
    >;
    Container container;
    size_t lastDigestId = 0;
public:
    bool tryInsertDigest(Sha512Digest digest) {
        auto& indexByHash = container.get<IndexByHash>();
        auto hashIt = indexByHash.equal_range((size_t)digest);

        for(auto it = hashIt.first; it != hashIt.second; ++ it) {
            if(it->digest == digest)
                return false;
        }

        container.insert(DigestInputRecord(std::move(digest), lastDigestId++));
        return true;
    }

    bool tryInsertInput(TrustKeyInput input) {
        auto& indexByHash = container.get<IndexByHash>();
        auto digest = input.calculate();
        auto hashIt = indexByHash.equal_range((size_t)*digest);

        for(auto it = hashIt.first; it != hashIt.second; ++ it) {
            if(it->digest == *digest) {
                if(it->input) //Input already set
                    return false;

                DigestInputRecord& digestInputRecord = const_cast<DigestInputRecord&>(*it);
                digestInputRecord.input = std::move(input);
                return true;
            }
        }

        return false;
    }

    void iterateByDigestIdIndex(std::function<void(const DigestInputRecord& digestInputRecord)> foo) {
        auto& indexByDigestId = container.get<IndexByDigestId>();
        for(auto it = indexByDigestId.begin(); it != indexByDigestId.end(); ++it)
            foo(*it);
    }
};


#endif //TRUSTKEYAPISERVER_DIGESTINPUTMULTIMAP_HPP
