#include "WebsocketServer.hpp"
#include "TrustKeyService.hpp"
#include "Helpers.hpp"
#include "Base64.hpp"

//Todo: PIMPl

using namespace websocketpp;

void WebsocketServer::on_open(connection_hdl hdl) {
    m_connections.insert(hdl);
    auto conPtr = server.get_con_from_hdl(hdl);
    if(conPtr) {
        auto uri = conPtr->get_uri();
        string uriString = uri->str();

        size_t startPos = uriString.find("q=");
        if(startPos != string::npos) {
            size_t endPos = uriString.find("&");
            string qString = uriString.substr(startPos + 2, endPos);
            conPtr->query[WebsocketMessageType::ROUND_SYNC] = uriString.find("round_sync") != string::npos;
            conPtr->query[WebsocketMessageType::HASH] = uriString.find("hashes") != string::npos;
            conPtr->query[WebsocketMessageType::INPUT] = uriString.find("inputs") != string::npos;
            conPtr->query[WebsocketMessageType::TRUST_KEY] = uriString.find("trust_keys") != string::npos;
            conPtr->query[WebsocketMessageType::MESSAGE] = uriString.find("messages") != string::npos;
            conPtr->query[WebsocketMessageType::SERVER_INFO] = uriString.find("server_info") != string::npos;
        }

        if(conPtr->query[WebsocketMessageType::SERVER_INFO])
            server.send(hdl, jsonGenerator.wsStandardMessage("server_info",
                         std::bind(&JsonGenerator::writeServerInfo, jsonGenerator, placeholders::_1, configuration)),
                    frame::opcode::TEXT);

        if(trustKeyService->getCurrRound() >= 0)
            server.send(hdl, jsonGenerator.roundSync(trustKeyService->getCurrTrustKeyTs(),
                                                     trustKeyService->getNextTrustKeyTs(),
                                                     h::currentTimeMs()), frame::opcode::TEXT);
    }
}

void WebsocketServer::on_close(connection_hdl hdl) {
    m_connections.erase(hdl);
}

void WebsocketServer::on_message(connection_hdl hdl, WsServer::message_ptr msg) {
    auto conPtr = server.get_con_from_hdl(hdl);

    auto boundHeaderWriter = std::bind(
            &JsonGenerator::writeMessageHeader,
            &jsonGenerator,
            placeholders::_1,
            "msg_response",
            conPtr->msgId,
            [](JsonGenerator::SbWriter &w) {}
    );

    ++conPtr->msgId;

    if(!conPtr)
        return;

    if(msg->get_opcode() == websocketpp::frame::opcode::TEXT) {
        string& payload = msg->get_raw_payload();

        Document jsonBody;

        auto sendValidationError = [&]() {
            server.send(hdl,
                        jsonGenerator.errorResponse(
                                ErrorCode::MESSAGE_BODY_VALIDATION_ERROR,
                                boundHeaderWriter
                        ),
                        websocketpp::frame::opcode::TEXT);
        };

        if (jsonBody.Parse(payload.c_str()).HasParseError() || !jsonValidator.validateDocument("ws_message", jsonBody)) {
            sendValidationError();
            return;
        }

        string messageType = jsonBody["type"].GetString();

        if(messageType == "post_b64input") {
            if(!jsonValidator.validateDocument("ws_post_b64input", jsonBody)) {
                sendValidationError();
                return;
            }

            const char* b64Str = jsonBody["b64input"].GetString();
            string bytes = Base64::decode(b64Str, strlen(b64Str));

            if(bytes.size() != configuration.inputLength) {
                sendValidationError();
                return;
            }

            unsigned char* data = (unsigned char *) malloc(bytes.size());
            memcpy(data, &bytes[0], bytes.size());

            trustKeyService->pushInput(TrustKeyInput(data, bytes.size()),
                [this, hdl, boundHeaderWriter](PushInputError error, size_t currTrustKeyId) {
                    service->post([this, error, currTrustKeyId, hdl, boundHeaderWriter]() {
                        server.send(
                                hdl,
                                jsonGenerator.postInputResponse(error, currTrustKeyId, boundHeaderWriter),
                                websocketpp::frame::opcode::TEXT
                        );
                    });
                }, false);

            return;
        }

        if(messageType == "post_hash") {
            if(!jsonValidator.validateDocument("ws_post_hash", jsonBody)) {
                sendValidationError();
                return;
            }

            string digestString = jsonBody["sha512_hash"].GetString();
            boost::optional<Sha512Digest> digest = Sha512Digest::fromHexString(digestString);

            if(!digest) {
                sendValidationError();
                return;
            }

            trustKeyService->pushHash(*digest, [this, hdl, boundHeaderWriter](size_t nextTrustKeyTs) {
                service->post([this, nextTrustKeyTs, hdl, boundHeaderWriter]() {
                    server.send(hdl, jsonGenerator.postHashResponse(nextTrustKeyTs, boundHeaderWriter), websocketpp::frame::opcode::TEXT);
                });
            }, false);

            return;
        }

        //Message is not handled
        sendValidationError();
    }
}

void WebsocketServer::broadcast(WebsocketMessageType t, std::string message) {
    WsServer::message_ptr m =
            messageManager->get_message(websocketpp::frame::opcode::TEXT, message.size() + 1);
    m->append_payload(message);

    service->post([m = std::move(m), t, this](){
        for (const auto &m_connection : m_connections) {
            auto conPtr = server.get_con_from_hdl(m_connection);
            if(conPtr) {
                try {
                    if(conPtr->query[t])
                        server.send(m_connection, m);
                }
                catch (websocketpp::exception e) {
                    bool debug = true;
                }
                catch (...) {
                    bool debug = true;

                    //Todo: debug
                }

            }
        }
    });
}

void WebsocketServer::start(bool detached) {
    assert(trustKeyService);

    jsonValidator.addScheme("ws_message", "ws_message.json");
    jsonValidator.addScheme("ws_post_b64input", "ws_post_b64input.json");
    jsonValidator.addScheme("ws_post_hash", "ws_post_hash.json");

    server.init_asio(&*service);

    server.set_open_handler(websocketpp::lib::bind(&WebsocketServer::on_open,this,::_1));
    server.set_close_handler(websocketpp::lib::bind(&WebsocketServer::on_close,this,::_1));
    server.set_message_handler(websocketpp::lib::bind(&WebsocketServer::on_message,this,::_1,::_2));
    server.clear_access_channels(websocketpp::log::alevel::frame_header | websocketpp::log::alevel::frame_payload);
    server.clear_access_channels(websocketpp::log::alevel::all);
    server.set_listen_backlog(64);

    auto workerFoo = [this]() {
        //Todo: solve termination problem

        server.listen(configuration.websocketServerPort);
        server.start_accept();
        server.run();
    };

    if(detached)
        std::thread(workerFoo).detach();
    else
        workerFoo();
}

void WebsocketServer::setTrustKeyService(TrustKeyService *trustKeyService) {
    WebsocketServer::trustKeyService = trustKeyService;
}