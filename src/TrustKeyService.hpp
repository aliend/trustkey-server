#ifndef API_TRUSTKEYSERVICE_HPP
#define API_TRUSTKEYSERVICE_HPP

#include <boost/asio.hpp>
#include <functional>
#include <random>

#include "TrustKeyConfiguration.hpp"
#include "Sha512.hpp"
#include "DeferredWriter.hpp"
#include "WebsocketServer.hpp"
#include "JsonGenerator.hpp"
#include "TrustKeyDb.hpp"
#include "ServerInputGenerator.hpp"
#include "MultiIndexDigestSet.hpp"

class HttpApiServer;

using PushHashCallback = std::function<void(size_t nextTrustKetId)>;
using PushInputCallback = std::function<void(PushInputError error, size_t currTrustKeyId)>;

class TrustKeyService {
    const TrustKeyConfiguration& configuration;
    const JsonGenerator& jsonGenerator;
    WebsocketServer& websocketServer;
    TrustKeyDb& trustKeyDb;
    HttpApiServer& httpApiServer;

    //Server input generator
    ServerInputGenerator serverInputGenerator;
    TrustKeyInput currRoundServerInput;
    TrustKeyInput nextRoundServerInput;

    boost::asio::io_service primaryService;
    size_t currTrustKeyTs;
    size_t nextTrustKeyTs;

    int currRound;

    std::unique_ptr<MultiIndexDigestSet> preparationDigestSet;
    std::unique_ptr<MultiIndexDigestSet> checkDigestSet;

    std::shared_ptr<DeferredWriter> hashesLogWriter;
    std::shared_ptr<DeferredWriter> inputsLogWriter;
    std::shared_ptr<DeferredWriter> inputsWriter;
    std::shared_ptr<DeferredWriter> sha512TrustKeyWriter;

    int32_t nInputs;
public:
    TrustKeyService(const TrustKeyConfiguration &configuration, const JsonGenerator &jsonGenerator,
                    WebsocketServer &websocketServer, TrustKeyDb &trustKeyDb, HttpApiServer &httpApiServer);
    size_t getCurrTrustKeyTs() const;
    size_t getNextTrustKeyTs() const;
    int getCurrRound() const;
    void post(std::function<void()> handler, size_t priority = 0);
    void beginRound(boost::asio::deadline_timer *d);
    void pushHash(Sha512Digest d, PushHashCallback, bool schedule = true);
    void pushInput(TrustKeyInput, PushInputCallback, bool schedule = true);
    void generateInputPostHash();
    void postServerInput();
    void start(bool detached = false);
};

#endif //API_TRUSTKEYSERVICE_HPP