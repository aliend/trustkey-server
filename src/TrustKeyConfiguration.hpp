#ifndef API_CONFIG_HPP
#define API_CONFIG_HPP

#include <string>

class TrustKeyConfiguration {
public:
    void loadFromFile(const std::string& fileName);
    std::string serverId;

    unsigned int httpServerPort;

    unsigned int websocketServerPort;
    unsigned int roundTime;
    unsigned int inputLength;

    unsigned int serverInputLength;
    mode_t fileMode;
    std::string storageDirPath;
    std::string trustKeyDbFileName;
    std::string hashesLogFileName;
    std::string inputsLogFileName;
    std::string inputsFileName;
    std::string sha512TrustKeyFileName;

    std::string version = "0.0.3";
    std::string build = std::string(__DATE__) + " " + __TIME__;
};

#endif //API_CONFIG_HPP
