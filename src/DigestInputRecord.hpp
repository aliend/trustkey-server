#ifndef TRUSTKEYAPISERVER_DIGESTINPUTRECORD_HPP
#define TRUSTKEYAPISERVER_DIGESTINPUTRECORD_HPP


#include "Sha512.hpp"

class DigestInputRecord {
public:
    DigestInputRecord() = delete;
    Sha512Digest digest;
    size_t digestId;

    size_t digestUMapHash() const {
        return digest;
    }

    boost::optional<TrustKeyInput> input;
    DigestInputRecord(Sha512Digest&& digest, size_t digestId):
            digest(digest),
            digestId(digestId) {

    }
};


#endif //TRUSTKEYAPISERVER_DIGESTINPUTRECORD_HPP
