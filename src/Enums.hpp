#ifndef TRUSTKEYAPISERVER_ENUMS_HPP
#define TRUSTKEYAPISERVER_ENUMS_HPP

#include <string>

enum class PushInputError {
    NO_ERROR,
    HASH_NOT_FOUND
};

enum WebsocketMessageType: size_t {
    ROUND_SYNC = 0,
    HASH = 1,
    INPUT = 2,
    TRUST_KEY = 3,
    MESSAGE = 4,
    SERVER_INFO = 5
};

const size_t WebsocketQueryArrayLength = 6;

enum class ErrorCode: int {
    CONTENT_LENGHT_ERROR = 2,
    WRONG_INPUT_LENGTH   = 3,
    MESSAGE_BODY_VALIDATION_ERROR = 4,
    HASH_NOT_FOUND = 5,
};

extern std::string errors[];

#endif //TRUSTKEYAPISERVER_ENUMS_HPP