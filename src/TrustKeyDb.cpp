#include <fstream>

#include "TrustKeyDb.hpp"
#include "TrustKeyService.hpp"

using namespace std;

void TrustKeyDb::loadTkDb() {
    string tkDbPath = configuration.storageDirPath +
                      "/" + configuration.trustKeyDbFileName;
    bool exists = ifstream(tkDbPath).good();

    //|ios_base::out||ios_base::app
    if(!exists) {
        fstream tkDbStream(tkDbPath, ios_base::out);

        cout << "Creating new TrustKey database... ";
        tkDbStream << "timestamp,hex(trust_key),n_inputs";
        tkDbStream.flush();
        tkDbStream.close();
    }
    else {
        fstream tkDbStream(tkDbPath, ios_base::in);
        cout << "Loading TrustKey database... ";
        cout.flush();

        std::string line;
        getline(tkDbStream, line); //Skip csv header
        size_t lineI = 1;
        while(getline(tkDbStream, line)) {
            string word;
            istringstream iss(line);
            time_t timestamp;
            string hex;
            size_t nInputs;

            auto throwReadError = [&]() {
                throw(string("Unable to read TrustKey DB: ") + to_string(lineI));
            };

            if(getline(iss, word, ','))
                timestamp = stoi(word);
            else
                throwReadError();

            if(!getline(iss, hex, ','))
                throwReadError();


            if(getline(iss, word, ',')) {
                nInputs = stoi(word);
            } else
                throwReadError();

            trustKeyDb.emplace(timestamp, move(hex), nInputs);
            ++lineI;
        }
        tkDbStream.close();
    }
    cout << "Done." << endl;

    cout << "Opening TrustKeyDb for append... ";
    cout.flush();

    tkDbWriteStream = ofstream(tkDbPath, ios_base::out | ios_base::app);
    bool isGood = tkDbWriteStream.good();
    if(isGood)
        cout << "Done." << endl;
    else
        throw("Unable to open TrustKeyDb for writing.");
}

void TrustKeyDb::insertTrustKey(TrustKeyDbEntry entry) {
    //Todo: подумать об синхронизации и публикации результатов только после записи в файл
    thread([entry = move(entry), this](){
        boost::upgrade_lock<boost::shared_mutex> lock(dbAccess);
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);

        stringstream ss;
        ss << "\n" << entry.timestamp << "," << entry.trustKeyHex << "," << entry.nInputs;

        tkDbWriteStream << ss.str();
        tkDbWriteStream.flush();

        trustKeyDb.insert(move(entry));
    }).detach();
}

void TrustKeyDb::trustKeyRange(time_t from, time_t to, size_t limit, TrustKeyDbQueryCallback cb) {
    boost::shared_lock<boost::shared_mutex> lock(dbAccess);

    set<TrustKeyDbEntry>::iterator it = trustKeyDb.lower_bound(TrustKeyDbEntry(from, "", 0));
    while(it != trustKeyDb.end()) {
        const TrustKeyDbEntry& entry = *it;
        cb(entry);
        ++it;
    }
}

void TrustKeyDb::setTrustKeyService(TrustKeyService *trustKeyService) {
    TrustKeyDb::trustKeyService = trustKeyService;
}

TrustKeyDb::TrustKeyDb(const TrustKeyConfiguration &configuration) : configuration(configuration) {}
