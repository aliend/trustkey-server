#ifndef API_DEFERREDWRITER_HPP
#define API_DEFERREDWRITER_HPP

#include <string>
#include <list>
#include <fcntl.h>
#include <sys/stat.h>
#include <thread>
#include <zconf.h>
#include <iostream>
#include <memory>
#include <mutex>
#include <boost/filesystem.hpp>

using namespace std;

class WriteTask {
public:
    const void* ptr;
    size_t len;

    WriteTask(const void *ptr, size_t len) : ptr(ptr), len(len) {}
};

//Todo: aio use, double buffering
class DeferredWriter: public std::enable_shared_from_this<DeferredWriter> {
    std::list<WriteTask> tasks;
    int fdout;
    mutex m;
    struct USE_CREATE_METHOD{};
    string filePath;
    mode_t fileMode;
public:
    void worker();

    DeferredWriter(USE_CREATE_METHOD, std::string fileName, mode_t fileMode)
            : filePath(fileName), fileMode(fileMode) {
    }

    static shared_ptr<DeferredWriter> create(std::string filePath, mode_t fileMode);

    void startWorker();

    void append(const void* ptr, size_t len) {
        m.lock();
        tasks.emplace_front(ptr, len);
        m.unlock();
    }

    void appendCopy(const void* ptr, size_t len) {
        m.lock();

        void* ptrDataCopy = malloc(len);
        memcpy(ptrDataCopy, ptr, len);

        tasks.emplace_front(ptrDataCopy, len);
        m.unlock();
    }

    DeferredWriter& operator <<(string s) {
        char* data = (char*)malloc(s.size());
        memcpy(data, s.c_str(), s.size());
        append(data, s.size());

        return *this;
    }
};


#endif //API_DEFERREDWRITER_HPP