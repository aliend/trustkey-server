#ifndef TRUSTKEYAPISERVER_TKDB_HPP
#define TRUSTKEYAPISERVER_TKDB_HPP

class TrustKeyService;

#include <string>
#include <set>
#include <boost/thread/shared_mutex.hpp>
#include "TrustKeyConfiguration.hpp"

class TrustKeyDbEntry {
public:
    time_t timestamp;
    std::string trustKeyHex;
    int32_t nInputs;
    TrustKeyDbEntry(time_t timestamp,
                    const std::string trustKeyHex,
                    int32_t nInputs)
            : timestamp(timestamp),
              trustKeyHex(trustKeyHex),
              nInputs(nInputs) {}

    bool operator<(const TrustKeyDbEntry& rhs) const {
        return timestamp < rhs.timestamp;
    }
};

using TrustKeyDbQueryCallback = std::function<void(const TrustKeyDbEntry&)>;

class TrustKeyDb {
    TrustKeyService* trustKeyService;
    const TrustKeyConfiguration& configuration;
public:
    TrustKeyDb(const TrustKeyConfiguration &configuration);

private:
    std::set<TrustKeyDbEntry> trustKeyDb;
    boost::shared_mutex dbAccess;
    std::ofstream tkDbWriteStream;
public:
    void loadTkDb();

    void setTrustKeyService(TrustKeyService *trustKeyService);
    void insertTrustKey(TrustKeyDbEntry entry);
    void trustKeyRange(time_t from, time_t to, size_t limit, TrustKeyDbQueryCallback cb);
};

#endif //TRUSTKEYAPISERVER_TKDB_HPP
