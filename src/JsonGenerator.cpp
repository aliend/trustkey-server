#include "JsonGenerator.hpp"
#include "Helpers.hpp"
#include "TrustKeyService.hpp"
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

using namespace std;
using namespace rapidjson;

string JsonGenerator::standardResponse(bool success, ObjectWriterCallback c) const {
    StringBuffer buffer;
    Writer<StringBuffer> w(buffer);
    w.StartObject();

    w.Key("success"); w.Bool(success);

    c(w);

    w.EndObject();

    return buffer.GetString();
}

std::string JsonGenerator::errorResponse(int errCode, string error, JsonGenerator::ObjectWriterCallback c) const {
    return standardResponse(false, [&](SbWriter& w) {
        w.Key("error_code"); w.Int(errCode);
        w.Key("error"); w.String(error.c_str());
        c(w);
    });
}

string JsonGenerator::wsStandardMessage(string type, JsonGenerator::ObjectWriterCallback c) const {
    StringBuffer buffer;
    Writer<StringBuffer> w(buffer);
    w.StartObject();

    w.Key("type"); w.String(type.c_str());

    c(w);

    w.EndObject();

    return buffer.GetString();
}

std::string JsonGenerator::errorResponse(ErrorCode errCode, JsonGenerator::ObjectWriterCallback c) const {
    return errorResponse((int)errCode, errors[(int)errCode], c); //Todo: possible optimization
}

string JsonGenerator::wsHash(string hashHex, double t) const {
    return wsStandardMessage("hash", [&](SbWriter &w) {
        w.Key("ts");
        w.Double(t);
        w.Key("hash");
        w.String(hashHex.c_str());
    });
}

std::string JsonGenerator::wsInput(std::string inputBase64, double t) const {
    return wsStandardMessage("input", [&](SbWriter &w) {
        w.Key("ts");
        w.Double(t);
        w.Key("input_base64");
        w.String(inputBase64.c_str());
    });
}

std::string JsonGenerator::wsTrustKey(size_t tkId, std::string trustKeyHex, double t) const {
    return wsStandardMessage("trust_key", [&](SbWriter &w) {
        w.Key("ts");
        w.Double(t);
        w.Key("sha512_hex");
        w.String(trustKeyHex.c_str());
        w.Key("trust_key_ts");
        w.Uint64(tkId);
    });
}

std::string JsonGenerator::roundSync(size_t currTrustKeyId, size_t nextTrustKeyId, double t) const {
    return wsStandardMessage("round_sync", [&](SbWriter &w) {
        w.Key("ts");
        w.Double(t);
        w.Key("current_trustkey_ts");
        w.Uint64(currTrustKeyId);
        w.Key("next_trustkey_ts");
        w.Uint64(nextTrustKeyId);
    });
}

void JsonGenerator::writeServerInfo(JsonGenerator::SbWriter &w, const TrustKeyConfiguration &configuration) const {
    w.Key("server_id"); w.String(configuration.serverId.c_str());
    w.Key("server_ts"); w.Double(h::currentTimeMs());

    w.Key("config"); w.StartObject(); {
        w.Key("round_time"); w.Uint64(configuration.roundTime);
        w.Key("input_length"); w.Uint64(configuration.inputLength);
        w.Key("server_input_length"); w.Uint64(configuration.serverInputLength);
    } w.EndObject();

    w.Key("version"); w.String(configuration.version.c_str());
    w.Key("build"); w.String(configuration.build.c_str());
}

void JsonGenerator::writeMessageHeader(JsonGenerator::SbWriter &w, std::string type, size_t msgId,
                                       ObjectWriterCallback c) const {
    w.Key("type"); w.String(type.c_str());
    w.Key("msg_id"); w.Uint64(msgId);
    c(w);
}

std::string JsonGenerator::postHashResponse(size_t nextTrustKeyTs, JsonGenerator::ObjectWriterCallback c) const {
    return standardResponse(true, [&](SbWriter& w) {
        w.Key("next_trustkey_ts"); w.Uint64(nextTrustKeyTs);
        c(w);
    });
}

std::string JsonGenerator::postInputResponse(PushInputError error,
                                             size_t currTrustKeyTs,
                                             JsonGenerator::ObjectWriterCallback c) const {
    if(error == PushInputError::HASH_NOT_FOUND)
        return errorResponse(ErrorCode::HASH_NOT_FOUND, c);

    return standardResponse(true, [&](SbWriter& w) {
        w.Key("curr_trustkey_ts"); w.Uint64(currTrustKeyTs);
        c(w);
    });
}