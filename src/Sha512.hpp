#ifndef API_SHA512CALCRESULT_HPP
#define API_SHA512CALCRESULT_HPP

#include <cstring>
#include <string>
#include <memory>
#include <openssl/sha.h>
#include <boost/optional.hpp>

class Sha512Digest;

class TrustKeyInput { //Todo: implement move semantic
public:
    unsigned char* ptr;
    size_t size;
    TrustKeyInput() = delete;

    friend void swap(TrustKeyInput& first, TrustKeyInput& second) {
        using std::swap;
        swap(first.ptr, second.ptr);
        swap(first.size, second.size);
    }

    TrustKeyInput(unsigned char *bytes, size_t size);

    bool operator==(const TrustKeyInput& oth) const {
        if(size != oth.size)
            return false;

        return memcmp(ptr, oth.ptr, size) == 0;
    }

    TrustKeyInput& operator=(TrustKeyInput copy) {
        using std::swap;
        swap(*this, copy);
        return *this;
    }

    std::unique_ptr<Sha512Digest> calculate() const;
};


class Sha512Digest { //Todo: same
    //Sha512Digest() {}
    friend class TrustKeyInput;
    Sha512Digest() {
        digest = static_cast<unsigned char*>(malloc(SHA512_DIGEST_LENGTH));
    }
public:
    static boost::optional<Sha512Digest> fromHexString(const std::string& str);

    Sha512Digest(const Sha512Digest& src) {
        digest = static_cast<unsigned char*>(malloc(SHA512_DIGEST_LENGTH));
        memcpy(digest, src.digest, SHA512_DIGEST_LENGTH);
    };

    Sha512Digest& operator=(Sha512Digest copy) {
        swap(*this, copy);
        return *this;
    }

    size_t hash() const {
        return *reinterpret_cast<size_t*>(digest);
    }

    operator size_t() const {
            return hash();
    }

    /*
    Sha512Digest& operator=(Sha512Digest&& src) {
        digest = src.digest;
    }
     */

    unsigned char* digest = nullptr;
    std::string toString() const;

    bool operator==(const Sha512Digest& oth) const {
        return memcmp(this->digest, oth.digest, SHA512_DIGEST_LENGTH) == 0;
    }

    friend void swap(Sha512Digest& first, Sha512Digest& second) {
        using std::swap;
        swap(first.digest, second.digest);
    }


    ~Sha512Digest() {
        if(digest)
            delete digest;
    }
};

namespace std
{
    template <>
    struct hash<Sha512Digest>
    {
        size_t operator()(Sha512Digest const & x) const noexcept
        {
            return *reinterpret_cast<const size_t*>(x.digest);
        }
    };
}

#endif //API_SHA512CALCRESULT_HPP