#include "TrustKeyService.hpp"
#include "Helpers.hpp"
#include "Base64.hpp"
#include "HttpApiServer.hpp"

#include <iostream>
#include <thread>
#include <boost/filesystem.hpp>

#define PRIORITY_MAX 1000
#define PRIORITY_LOW 0

using namespace boost::asio;
using namespace std;
namespace fs = boost::filesystem;
#include "boost/date_time/posix_time/posix_time.hpp"
inline string getOutputDir(time_t tkId) {
    string p2 = to_string(tkId % 1000);
    tkId /= 1000;
    string p1 = to_string(tkId % 1000);
    tkId /= 1000;

    return to_string(tkId) + "/" + p1 + "/" + p2;
};

TrustKeyService::TrustKeyService(const TrustKeyConfiguration &configuration, const JsonGenerator &jsonGenerator,
                                 WebsocketServer &websocketServer, TrustKeyDb &trustKeyDb, HttpApiServer& httpApiServer)
        : configuration(configuration),
          jsonGenerator(jsonGenerator),
          websocketServer(websocketServer),
          trustKeyDb(trustKeyDb),
          httpApiServer(httpApiServer),
          currRoundServerInput(nullptr, 0),
          nextRoundServerInput(nullptr, 0),
          serverInputGenerator(configuration.serverInputLength),
          currTrustKeyTs(0), nextTrustKeyTs(0) {
    websocketServer.setTrustKeyService(this);
    trustKeyDb.setTrustKeyService(this);
    httpApiServer.setTrustKeyService(this);
    currRound = -1;
    preparationDigestSet = make_unique<MultiIndexDigestSet>();
    checkDigestSet = make_unique<MultiIndexDigestSet>();
}

void TrustKeyService::start(bool detached) {
    boost::asio::io_service::work fakeWork(primaryService);

    auto workerFoo = [this]() {
        time_t currTime = time(nullptr);

        size_t nextRound = currTime / configuration.roundTime + 1;
        size_t nextCycleTime = nextRound * configuration.roundTime;
        currTrustKeyTs = nextCycleTime;

        double currTimeMs = h::currentTimeMs();
        double awaitTimeout = (double)nextCycleTime - currTimeMs;

        cout << "Spawning main worker" << endl
             << "current time: " << currTime << endl
             << "delay before start: " << awaitTimeout << endl;

        deadline_timer* deadlineTimer = new deadline_timer(primaryService);
        deadlineTimer->expires_at(boost::posix_time::from_time_t(nextCycleTime));
        deadlineTimer->async_wait(bind(&TrustKeyService::beginRound, this, deadlineTimer));

        primaryService.run();
    };

    if(detached)
        thread(workerFoo).detach();
    else
        workerFoo();
}

void TrustKeyService::beginRound(boost::asio::deadline_timer *d) {
    delete d;
    ++currRound;

    time_t currTime = time(nullptr);

    size_t prevTrustKeyTs = currTrustKeyTs;

    currTrustKeyTs = currTime / configuration.roundTime * configuration.roundTime + configuration.roundTime;
    //OS may schedule us a little bit earlier. We need to recheck
    //if ts of current trustkey actually bigger then previous

    if(currTrustKeyTs <= prevTrustKeyTs)
        currTrustKeyTs = prevTrustKeyTs + configuration.roundTime;

    nextTrustKeyTs = currTrustKeyTs + configuration.roundTime;

    cout << h::sCurrentTimeMs() << " / "
            << "Curr " << currRound
            << ". TrustKeyTs: " << currTrustKeyTs
            << " / NextTrustKeyTs: " << currTrustKeyTs + configuration.roundTime << endl;

    //Finalization of previous round

    if(currRound == 1)
        checkDigestSet.swap(preparationDigestSet);

    if(currRound >= 2) { //!
        postServerInput();

        SHA512_CTX ctx;
        SHA512_Init(&ctx);

        checkDigestSet->iterateByDigestIdIndex([&](const DigestInputRecord& record){
            if(record.input) {
                const TrustKeyInput& input = *record.input;
                inputsWriter->append(input.ptr, input.size);
                SHA512_Update(&ctx, input.ptr, input.size);
            }
        });

        unsigned char digest[SHA512_DIGEST_LENGTH];
        SHA512_Final(digest, &ctx);

        char* mdString = static_cast<char*>(operator new(SHA512_DIGEST_LENGTH * 2 + 1));
        for (int i = 0; i < SHA512_DIGEST_LENGTH; i++)
            sprintf(&mdString[i*2], "%02X", (unsigned int)digest[i]);

        sha512TrustKeyWriter->append(mdString, SHA512_DIGEST_LENGTH*2);
        trustKeyDb.insertTrustKey(TrustKeyDbEntry(currTrustKeyTs, string(mdString), nInputs));
        websocketServer.broadcast(WebsocketMessageType::TRUST_KEY,
                                  jsonGenerator.wsTrustKey(currTrustKeyTs,
                                                           string(mdString),
                                                           h::currentTimeMs()));

        checkDigestSet.swap(preparationDigestSet);
        preparationDigestSet = make_unique<MultiIndexDigestSet>();
    }

    //Next round

    string nextTkOutputDir = getOutputDir(nextTrustKeyTs);
    string currTkOutputDir = getOutputDir(currTrustKeyTs);
    fs::path nextTkBasePath = fs::path(configuration.storageDirPath) / fs::path(nextTkOutputDir);
    fs::path currTkBasePath = fs::path(configuration.storageDirPath) / fs::path(currTkOutputDir);

    hashesLogWriter = DeferredWriter::create(
            (nextTkBasePath / fs::path(configuration.hashesLogFileName)).c_str(),
            configuration.fileMode
    );

    *hashesLogWriter << "timestamp" << "," << "hex(hash)";
    hashesLogWriter->startWorker();

    if(currRound >= 1) {
        inputsLogWriter = DeferredWriter::create(
                (currTkBasePath / fs::path(configuration.inputsLogFileName)).c_str(),
                configuration.fileMode
        );

        *inputsLogWriter << "timestamp" << "," << "base64(input)";

        inputsLogWriter->startWorker();

        inputsWriter = DeferredWriter::create(
                (currTkBasePath / fs::path(configuration.inputsFileName)).c_str(),
                configuration.fileMode
        );

        inputsWriter->startWorker();

        sha512TrustKeyWriter = DeferredWriter::create(
                (currTkBasePath / fs::path(configuration.sha512TrustKeyFileName)).c_str(),
                configuration.fileMode
        );

        sha512TrustKeyWriter->startWorker();
    }

    nInputs = 0;

    websocketServer.broadcast(WebsocketMessageType::ROUND_SYNC,
                              jsonGenerator.roundSync(currTrustKeyTs, nextTrustKeyTs, h::currentTimeMs()));

    generateInputPostHash();

    //Schedule next round
    deadline_timer* deadlineTimer = new deadline_timer(primaryService);
    deadlineTimer->expires_at(boost::posix_time::from_time_t(currTrustKeyTs));
    deadlineTimer->async_wait(bind(&TrustKeyService::beginRound, this, deadlineTimer));
}

void TrustKeyService::pushHash(Sha512Digest d, PushHashCallback cb, bool schedule) {
    auto foo = [this, d(move(d)), cb]() {
        preparationDigestSet->tryInsertDigest(move(d));
        double currentTime = h::currentTimeMs();
        string hashVal = d.toString();
        *hashesLogWriter << "\n" << h::sCurrentTimeMs(currentTime) << "," << hashVal;
        websocketServer.broadcast(WebsocketMessageType::HASH, jsonGenerator.wsHash(move(hashVal), currentTime));

        cb(nextTrustKeyTs);
    };

    schedule ? post(foo) : foo();
}

void TrustKeyService::pushInput(TrustKeyInput t, PushInputCallback cb, bool schedule) {
    //todo: calculate hash on client's thread
    auto foo = [this, t, cb(move(cb))](){
        PushInputError error = PushInputError::NO_ERROR;

        unique_ptr<Sha512Digest> calculated = t.calculate();

        bool hasInserted = checkDigestSet->tryInsertInput(t);
        if(hasInserted) {
            string b64Value = Base64::encode(t.ptr, t.size);
            double currentTime = h::currentTimeMs();
            *inputsLogWriter << "\n" << h::sCurrentTimeMs(currentTime) << "," << b64Value;
            websocketServer.broadcast(WebsocketMessageType::INPUT,
                                      jsonGenerator.wsInput(move(b64Value), currentTime));
        } else
            error = PushInputError::HASH_NOT_FOUND;

        cb(error, currTrustKeyTs);
    };

    schedule ? post(foo) : foo();
}

void TrustKeyService::post(std::function<void()> handler, size_t priority) {
    //Todo: priority queue use

    primaryService.post(handler);
}

size_t TrustKeyService::getCurrTrustKeyTs() const {
    return currTrustKeyTs;
}

size_t TrustKeyService::getNextTrustKeyTs() const {
    return nextTrustKeyTs;
}

void TrustKeyService::postServerInput() {
    pushInput(currRoundServerInput, [](PushInputError e, size_t ss) { }, false);
}

void TrustKeyService::generateInputPostHash() {
    currRoundServerInput = nextRoundServerInput;
    nextRoundServerInput = serverInputGenerator.generate();
    unique_ptr<Sha512Digest> digestPtr = nextRoundServerInput.calculate();
    pushHash(*digestPtr, [](size_t) { }, false);
}

int TrustKeyService::getCurrRound() const {
    return currRound;
}

