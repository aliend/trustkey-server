#ifndef CONVSERV_JSONVALIDATOR_H
#define CONVSERV_JSONVALIDATOR_H

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/schema.h"
#include <fstream>
#include <map>

using namespace rapidjson;

class WrongSchemeException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Wrong scheme!";
    }
};

class JsonRequestBodyValidator {
    std::map<std::string, SchemaDocument> schemes;
public:
    void addScheme(std::string schemeKey, std::string schemeFileName);
    bool validateDocument(std::string schemeKey, const Document& document);
};

#endif //CONVSERV_JSONVALIDATOR_H
